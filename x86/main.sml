(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)


structure Main =
struct

fun withOpenFile fname f =
  let val out = TextIO.openOut fname
   in (f out before TextIO.closeOut out)
  end

fun compile (infile, outfile) =
    let
        val absyn  = Parse.parse infile
        val tabsyn = Semant.transProg absyn
        val oabsyn = AugmentOffset.offsetAugmentProg tabsyn
        fun emitasm out = let
	    val ll_prog = LLCodegen.codegen_prog oabsyn
	    val x86_prog = X86Backend.compile_prog ll_prog
	    val asm_s = X86.string_of_prog x86_prog
        in TextIO.output (out, asm_s)
        end
    in
        if !ErrorMsg.anyErrors then OS.Process.exit 1
        else (withOpenFile outfile emitasm)
    end



fun exportedFn (self, [infile,outfile]) = (compile (infile,outfile); 0)
  | exportedFn (self, _) = (print "Expects arguments <infile> <outfile>"; ~1)

end (* Main *)
