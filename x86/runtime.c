// AU Compilation course
// Please do not distribute
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

int tigermain(void *, int64_t);

int64_t arrLenError(int64_t len) {
    printf("Error: attempt to create array with negative length (%" PRId64 "\n)",len);
    exit(1);
    return 0; /* keeping gcc happy */
}

struct array {
    int64_t size;
    void* arr;
};

void *initArray(int64_t size, int64_t elem_size, void* init) {
    int64_t i;
    void *a;
    void *p;

    if (size < 0) arrLenError(size);
    a = calloc(size, elem_size);
    p = a;

    for (i = 0; i < size; ++i) {
        memcpy(p, init, elem_size);
        p += elem_size;
    }

    return a;
}

int64_t arrInxError(int64_t index) {
    printf("Error: array index (%" PRId64 ") out of range\n", index);
    exit(1);
    return 0; /* keeping gcc happy */
}

void *allocRecord(int64_t size) {
    return calloc (size, 1);
}

int64_t recFieldError() {
    puts("Error: record field lookup applied to nil");
    exit(1);
    return 0; /* keeping gcc happy */
}

struct string {
    int64_t length;
    unsigned char chars[1];
};

int64_t stringEqual(struct string *s, struct string *t) {
    int64_t i;
    if (s == t)
        return 1;
    if (s->length != t->length)
        return 0;
    for (i = 0; i < s->length; i++)
        if (s->chars[i] != t->chars[i])
            return 0;
    return 1;

}

int64_t stringNotEq(struct string *s, struct string *t) {
    return !stringEqual(s, t);
}

int64_t stringLessEq(struct string *s, struct string *t) {
    int64_t i,len;
    if (s == t)
        return 1;
    len = s->length <= t->length ? s->length : t->length;
    for (i = 0; i < len; i++) {
        if (s->chars[i] < t->chars[i]) return 1;
        if (s->chars[i] > t->chars[i]) return 0;
        /* s->chars[j] == t->chars[j] for all j, 0<=j<=i */
    }
    return (s->length <= t->length);
}

int64_t stringLess(struct string *s, struct string *t) {
    return !stringLessEq(t, s);
}

int64_t stringGreater(struct string *s, struct string *t) {
    return !stringLessEq(s, t);
}

int64_t stringGreaterEq(struct string *s, struct string *t) {
    return stringLessEq(t, s);
}

void print(void *static_link, struct string *s) {
    int64_t i;
    unsigned char *p = s->chars;
    for (i = 0; i < s->length; i++, p++) {
        putchar(*p);
    }
}

void flush(void *static_link) {
    fflush(stdout);
}

struct string consts[256];
struct string empty = { 0, "" };

int main(int argc, char *argv[]) {
    int i;
    int result;

    for (i = 0; i < 256; i++) {
        consts[i].length = 1;
        consts[i].chars[0] = i;
    }

    /* args to tigermain: 0 is the static link, 1000 is unused, but
     * easy to see on the stack, nice when debugging frames */
    result = tigermain(0, 1000);
    return result;
}

int64_t ord(void *static_link, struct string *s) {
    if (s->length == 0)
        return -1;
    else {
        return s->chars[0];
    }
}

struct string *chr(void *static_link, int64_t i) {
    struct string *res;
    if (i < 0 || i >= 256) {
        printf("Error: chr(%" PRId64 ") out of range\n", i);
        exit(1);
    }
    res = consts + i;
    return consts + i;
}

int64_t size(void *static_link, struct string *s) {
    return s->length;
}

struct string *substring(void *static_link, struct string *s, int64_t first, int64_t n) {
    if (first < 0 || first + n > s->length) {
        printf("Error: substring([%" PRId64 "], %" PRId64 ", %" PRId64 ") out of range\n", s->length, first, n);
        exit(1);
    }
    if (n == 1) {
        return consts + s->chars[first];
    }

    struct string *t = (struct string *) malloc(sizeof(int64_t) + n * sizeof(char));
    int64_t i;
    t->length = n;
    for (i = 0; i < n; i++)
        t->chars[i] = s->chars[first + i];
    return t;
}

struct string *concat(void *static_link, struct string *a, struct string *b) {
    if (a->length == 0)
        return b;
    else if (b->length == 0)
        return a;
    else {
        int64_t i, n = a->length + b->length;
        struct string *t = (struct string *) malloc(sizeof(int64_t) + n * sizeof(char));
        t->length = n;
        for (i = 0; i < a->length; i++)
            t->chars[i] = a->chars[i];
        for(i = 0; i < b->length; i++)
            t->chars[i + a->length] = b->chars[i];
        return t;
    }
}

int64_t not(void *static_link, int64_t i) {
    return !i;
}

struct string *getChar(void *static_link) {
    int i = getc(stdin);
    if (i == EOF)
        return &empty;
    else
        return consts + i;
}

void exit_tig(void *static_link, int64_t status) {
    exit(status);
}
