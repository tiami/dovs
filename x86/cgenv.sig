signature CGENV =
sig

datatype enventry
  = VarEntry of
    { ty     : Types.ty
    , escape : bool ref
    , level  : int
    , offset : int
    }
  | FunEntry of
    { formals: Types.ty list
    , result : Types.ty
    , gid    : ll.gid
    , sl_type: ll.ty
    , level  : int }

val baseTenv: (Types.ty * ll.ty)  Symbol.table
val baseVenv: enventry Symbol.table

end
