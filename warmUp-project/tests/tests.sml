val prog2 = 
    ( s = 52; t = (2 + 10  s); n = (print(s, 3t), s+t) print (t-2s) )
    G.CompoundStm (
        G.AssignStm( s, G.OpExp(G.NumExp 5, G.Div, G.NumExp 2)),
        G.CompoundStm(
        G.AssignStm(t, G.OpExp(G.NumExp 2, G.Plus, (G.OpExp(G.NumExp 10, G.Times, G.IdExp s)))),
        G.CompoundStm(
            G.AssignStm(n, G.EseqExp (
                G.PrintStm [G.IdExp s, G.OpExp(G.NumExp 3, G.Times, G.IdExp t)],
                G.OpExp (G.IdExp s, G.Plus, G.IdExp t))),
            G.PrintStm [G.OpExp (G.IdExp t, G.Minus, G.OpExp(G.NumExp 2, G.Times, G.IdExp s))])))


val prog3 = 
    ( a = 15; b = 12; a2 = a; print(a, b, a2) )
    G.CompoundStm (
	    G.CompoundStm (
	        G.AssignStm(a, G.NumExp 15), G.AssignStm(b, G.NumExp 12)),
	    G.CompoundStm(
            G.AssignStm(a2, G.IdExp a), 
            G.PrintStm [G.IdExp a, G.IdExp b, G.IdExp a2]))


val prog4 = 
    ( a= 4; b= 42; z= 10; print(a,b,za); )
    G.CompoundStm (
        G.AssignStm(a, G.NumExp 4),
            G.CompoundStm (
                G.AssignStm(b, G.NumExp 42),
                G.CompoundStm(
                    G.AssignStm(z, G.NumExp 10),
                    G.PrintStm([G.IdExp a, G.IdExp b, G.OpExp(G.IdExp z, G.Div, G.IdExp a)]))))

val prog5 = 
    ( b= (4 + 4)  2; f= 10-2; print(b,f) )
    G.CompoundStm(
        G.AssignStm(b, G.OpExp((G.OpExp(G.NumExp 4, G.Plus, G.NumExp 4), G.Times, G.NumExp 2))),
        G.CompoundStm(
            G.AssignStm(f, G.OpExp(G.NumExp 10, G.Minus, G.NumExp 2)),
            G.PrintStm([G.IdExp b, G.IdExp f])))

val prog6 = 
    ( print((a = (k = 19  3; (h = 5; 3)); h + a) )
    G.PrintStm [G.EseqExp(
                    G.AssignStm(a, 
                        G.EseqExp(
                            G.AssignStm(k, G.OpExp(G.NumExp 19, G.Div, G.NumExp 3)), 
                        G.EseqExp(
                            G.AssignStm(h, G.NumExp 5),
                            G.NumExp 3))), 
		    G.OpExp(G.IdExp h, G.Plus, G.IdExp a))]

val prog7 = 
    ( Call-by-value 4, 5, 1, 2, 3, 6, 7, 8 )
    G.PrintStm([    G.NumExp 1, 
                    G.NumExp 2, 
                    G.NumExp 3, 
                    G.EseqExp(
                        G.PrintStm([G.NumExp 4, G.NumExp 5]), 
                        G.NumExp 6), 
                    G.NumExp 7, 
                    G.NumExp 8])
			  
val prog8 = 
    ( a = 0, b= 2a Expected output error because of division with 0 )
    G.CompoundStm(
        G.AssignStm(a, G.NumExp 0),
        G.AssignStm(b,
            G.OpExp(G.NumExp 2, G.Div, G.IdExp a)))