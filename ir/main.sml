(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)


structure Main =
struct

fun withOpenFile fname f =
  let val out = TextIO.openOut fname
   in (f out before TextIO.closeOut out)
  end

fun compile (infile, outfile) =
    let
        val absyn  = Parse.parse infile
        val tabsyn = Semant.transProg absyn
        val oabsyn = AugmentOffset.offsetAugmentProg tabsyn
        fun emitll out = let val ll_s = LLCodegen.codegen_ll oabsyn
                          in TextIO.output (out, ll_s)
                         end
    in
        if !ErrorMsg.anyErrors then OS.Process.exit 1
        else (withOpenFile outfile emitll)
    end



fun exportedFn (self, [infile,outfile]) = (compile (infile,outfile); 0)
  | exportedFn (self, _) = (print "Expects arguments <infile> <outfile>"; ~1)

end (* Main *)
