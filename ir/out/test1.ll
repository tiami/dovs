%Ttigermain0 = type { i1 }

define i64 @tigermain () {
 %Ulocals1 = alloca %Ttigermain0
 %Ubinop2 = icmp ne i64 1, 2
 ret i64 %Ubinop2
}

declare i8* @allocRecord(i64)
declare i8* @initArray (i64, i64, i8*)
declare i64 @stringEqual (i8*, i8*)
declare i64 @stringNotEq (i8*, i8*)
declare i64 @stringLess (i8*, i8*)
declare i64 @stringLessEq (i8*, i8*)
declare i64 @stringGreater (i8*, i8*)
declare i64 @stringGreaterEq (i8*, i8*)
declare void @print    (i8*, i8*)
declare void @flush    (i8*)
declare i8*  @getChar  (i8*)
declare i64  @ord      (i8*, i8*)
declare i8*  @chr      (i8*, i64)
declare i64  @size     (i8*, i8*)
declare i8*  @substring(i8*, i8*, i64, i64)
declare i8*  @concat   (i8*, i8*, i8*)
declare i64  @not      (i8*, i64)
declare void @exit     (i8*, i64)
target triple = "x86_64-pc-linux-gnu"
