(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)

(* LLVM--: a simplified subset of the LLVM IR,
  based on  S. Zdancewic's LLVMlite *)


structure ll =
struct

structure S = Symbol

type uid = S.symbol (* Local identifiers  *)
type gid = S.symbol (* Global identifiers *)
type tid = S.symbol (* Named types        *)
type lbl = S.symbol (* Labels             *)


(* LLVM IR types *)
datatype ty
  = Void                (* void                *)
  | I1 | I8 | I64       (* integer types       *)
  | Ptr of ty           (* t*                  *)
  | Struct of ty list   (* {t1, t2, ... , tn } *)
  | Array of int * ty   (* [NNN x t]           *)
  | Fun of fty          (* t1, ...., tn -> tr  *)
  | Namedt of tid       (* named type aliases  *)

(* Function type: argument types and return type *)
withtype fty = ty list * ty


(* Syntactic values *)
datatype operand
  = Null            (* null pointer        *)
  | Const of int    (* integer constant    *)
  | Gid of gid      (* A global identifier *)
  | Id of uid       (* A local identifier  *)


(* Binary operations *)
datatype bop = Add | Sub | Mul | SDiv | Shl | Lshr | Ashr | And | Or | Xor

(* Comparison operators *)
datatype cnd = Eq | Ne | Slt | Sle | Sgt | Sge

(* Instructions *)
datatype insn
  = Binop of bop * ty * operand * operand      (* bop ty %o1, o2                     *)
  | Alloca of ty                               (* alloca ty                          *)
  | Load of ty * operand                       (* load ty %u                         *)
  | Store of ty * operand * operand            (* store ty %t, ty* %u                *)
  | Icmp of cnd * ty * operand * operand       (* icmp %s ty %s, %s                  *)
  | Call of ty * operand * (ty * operand) list (* fn (%1, %2, ...)                   *)
  | Bitcast of ty * operand * ty               (* bitcast ty1 %u to ty2              *)
  | Gep of ty * operand * operand list         (* getelementptr ty* %u, i64 %vi, ... *)
  | Zext of ty * operand * ty                  (* zext ty1 %o to ty2                 *)
  | Ptrtoint of ty * operand * ty              (* ptrtoint ty1 %o to y2              *)

(* Block terminators *)
datatype terminator
  = Ret of ty * operand option (* ret i64 %s                     *)
  | Br of lbl                  (* br label %lbl                  *)
  | Cbr of operand * lbl * lbl (* br i1 %s, label %l1, label %l2 *)

(* Basic blocks *)

type block = { insns: (uid option * insn) list, terminator: terminator }

(* Control Flow Graph: a pair of an entry block and a set of labeled blocks *)
type cfg = block * (lbl * block) list

(* Function declarations *)
type fdecl = { fty: fty, param : uid list, cfg: cfg }


(* Initializers for global data *)
datatype ginit
  = GNull                      (* null literAL *)
  | GGid of gid                (* reference to another global *)
  | GInt of int                (* global integer value        *)
  | GString of string          (* constant global string      *)
  | GArray of gdecl list       (* global array                *)
  | GStruct of gdecl list      (* global struct               *)

(* Global declaration *)
withtype gdecl = ty * ginit

(* LLVM-- programs *)
type prog
     = { tdecls: (tid * ty) list       (* named types *)
       , gdecls: (gid * gdecl) list    (* global data *)
       , fdecls: (gid * fdecl) list    (* code        *)
       }

(* Serialization ----------------------------------------------------------- *)
exception NotImplemented
exception SerializationError

fun mapcat s f l = (String.concatWith s) (map f l)

infix 3 ^^
fun s  ^^ t = if s = "" orelse t = "" then "" else s ^ t
fun prefix p f a = p ^ f a

val concwsp = String.concatWith " "
fun parens s   = "("^s^")"
fun brackets s = "["^s^"]"
fun braces s   = "{"^s^"}"




fun string_of_ty (t:ty):string =
  case t of
      Void         => "void"
    | I1           => "i1"
    | I8           => "i8"
    | I64          => "i64"
    | Ptr t'       => (string_of_ty t')^"*"
    | Struct ts    => "{ " ^ (mapcat ", " string_of_ty ts) ^ " }"
    | Array (n, t) => "[" ^  (Int.toString n) ^ " x " ^  (string_of_ty t) ^ "]"
    | Fun (ts, t)  => (string_of_ty t) ^  "( " ^ (mapcat ", " string_of_ty ts) ^ " )"
    | Namedt s     => "%" ^ (S.name s)

val sot = string_of_ty


fun string_of_operand (opr:operand): string =
  case opr of
      Null      => "null"
    | Const i   => Int.toString i
    | Gid g     => "@" ^ (S.name g)
    | Id u      => "%" ^ (S.name u)

val soo = string_of_operand

fun soop (t: ty, v: operand) : string =
  (sot t) ^ " " ^ ( soo v)

fun string_of_bop (b: bop): string =
  case b of
      Add => "add" | Sub  => "sub"  | Mul => "mul"   |
      Shl => "shl" | Lshr => "lshr" | Ashr => "ashr" |
      And => "and" | Or   => "or"   | Xor => "xor"  | SDiv => "sdiv"

fun string_of_cnd (c:cnd): string =
    case c of
      Eq => "eq"   | Ne => "ne"   | Slt => "slt" |
      Sle => "sle" | Sgt => "sgt" | Sge => "sge"


fun string_of_gep_index (opr: operand): string =
  case opr of
      Const i => "i32 " ^ Int.toString i
    | opr'    => "i64 " ^ soo opr'

fun string_of_insn (ins:insn) : string =
    case ins of
	Binop (b, t, o1, o2)  => concwsp [ string_of_bop b
					 , sot t, soo o1^",", soo o2 ]
      | Alloca t              => "alloca " ^ (sot t)
      | Load (t, opr)         => concwsp [ "load"
					 , sot t ^"," , sot (Ptr t), soo opr]
      | Store (t, os, od)     => concwsp [ "store"
					 , sot t, soo os ^",", sot (Ptr t), soo od]
      | Icmp (c, t, o1, o2)   => concwsp [ "icmp"
					 , string_of_cnd c, sot t, soo o1^",", soo o2]
      | Call (t, opr, oa)     => concwsp [ "call"
					 , sot t
					 , soo opr
					 , parens (mapcat ", " soop oa) ]
      | Bitcast (t1, opr, t2) => concwsp [ "bitcast"
					 , sot t1
					 , soo opr
					 , "to"
					 , sot t2]
      | Gep (t, opr, oi)      => concwsp [ "getelementptr"
					 , sot t^","
					 , sot (Ptr t)
					 , soo opr ^","
					 , mapcat ", " string_of_gep_index oi]

      | Zext (t, o1, t2)      => concwsp [ "zext"
					 , sot t
					 , soo o1
					 , "to"
					 , sot t2]
(*      | Trunc (t, o1, t2)      => concwsp [ "trunc"
					 , sot t
					 , soo o1
					 , "to"
					 , sot t2] *)
      | Ptrtoint (t, o1, t2)  => concwsp [ "ptrtoint"
					 , sot (Ptr t)
					 , soo o1
					 , "to"
					 , sot t2]



fun string_of_named_insn (u: uid option, i: insn): string =
  case u of
      NONE     => string_of_insn i
   |  SOME u'  => "%"^(S.name u') ^ " = " ^ (string_of_insn i)

fun string_of_terminator (tr:terminator): string =
  case tr of
      Ret (_, NONE)     => "ret void"
    | Ret (t, SOME opr) => concwsp ["ret", sot t, soo opr]
    | Br l              => concwsp ["br", "label", "%"^(S.name l)]
    | Cbr (opr, l, m)   => concwsp [ "br"
				   , "i1" , soo opr ^ ","
				   , "label" , "%"^S.name l ^ ","
				   , "label" , "%"^S.name m]

fun string_of_block (b:block) : string =
  (mapcat "\n" (prefix " " string_of_named_insn) (#insns b) ^^ "\n")
  ^ (prefix " " string_of_terminator) (#terminator b)

fun string_of_cfg ((e, bs): cfg): string =
  let fun string_of_named_block (l, b) = (S.name l ) ^ ":\n" ^ string_of_block b
  in (string_of_block e) ^ "\n" ^ ( (mapcat "\n" string_of_named_block bs) ^^ "\n" )
  end

fun string_of_named_fdecl ( g: gid, f: fdecl): string =
  let fun string_of_arg (t, u) = (sot t) ^ " %"^(S.name u)
      val (ts, t) = #fty f
  in concwsp [ "define"
	     , sot t
	     , "@"^(S.name g)
	     , parens ( mapcat ", " string_of_arg (ListPair.zip (ts, #param f)))
	     , "{\n"^ (string_of_cfg (#cfg f) ) ^ "}\n"]
  end

(* converts strings to LL-compatible format *)
fun ll_encode s =
    let
        fun ase [] = []
          | ase (#"\\"::cs) = #"\\" :: #"\\" :: (ase cs)
          | ase (#"\""::cs) = #"\\" :: #"\"" :: (ase cs)
          | ase (c::cs) =
            let
                val ordc = ord(c)
                val charsc = StringCvt.padLeft #"0" 2 (Int.fmt StringCvt.HEX ordc)
            in
              	if 32 <= ordc andalso ordc < 128 then c :: (ase cs)
                else (#"\\" :: (explode charsc)) @ (ase cs)
            end
    in
      	(implode o ase o explode) s
    end


fun string_of_ginit ( gi: ginit): string =
  case gi of
      GNull         => "null"
    | GGid g        => "@"^ (S.name g)
    | GInt i        => Int.toString i
    | GString s     => "c\"" ^ (ll_encode s) ^ "\""
    | GArray gis    => brackets (mapcat ", " string_of_gdecl gis)
    | GStruct gis   => braces (mapcat ", " string_of_gdecl gis)
and string_of_gdecl ((t, gi):gdecl): string =
    concwsp [sot t, string_of_ginit gi]

fun string_of_named_gdecl (g : gid, gd : gdecl) : string =
  "@"^(S.name g) ^ " = global " ^ (string_of_gdecl gd)

fun string_of_named_tdecl (tid, ty)    =
  "%" ^ (S.name tid) ^ " = type " ^ (sot ty)


fun string_of_prog (p: prog): string =
  (mapcat "\n" string_of_named_tdecl (#tdecls p) ^^ "\n\n") ^
  (mapcat "\n" string_of_named_gdecl (#gdecls p) ^^ "\n\n") ^
  (mapcat "\n" string_of_named_fdecl (#fdecls p))



end
