	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$48, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	subq	$8, %rsp
	movq	%rsp, -16(%rbp)
	movq	$1, %r9
	movq	$0, %r10
	cmpq	%r10, %r9
	movq	$0, -24(%rbp)
	setne	-24(%rbp)
	movq	-24(%rbp), %r9
	movq	$0, %r10
	cmpq	%r9, %r10
	jne	Lthen2
	jmp	Lelse3
	.text
Lthen2:
	movq	$5, %rbx
	movq	$2, %rcx
	addq	%rbx, %rcx
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %r9
	movq	-32(%rbp), %r10
	movq	%r10, (%r9)
	jmp	Lmerge4
	.text
Lelse3:
	movq	$5, %rbx
	movq	$6, %rcx
	addq	%rbx, %rcx
	movq	%rcx, -40(%rbp)
	movq	-8(%rbp), %r9
	movq	-40(%rbp), %r10
	movq	%r10, (%r9)
	jmp	Lmerge4
	.text
Lmerge4:
	movq	-8(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	