	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$40, %rsp
	subq	$24, %rsp
	movq	%rsp, -8(%rbp)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	$15, %r10
	movq	%r10, (%r9)
	movq	-8(%rbp), %r9
	addq	$2, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	$20, %r10
	movq	%r10, (%r9)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -32(%rbp)
	movq	-32(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	